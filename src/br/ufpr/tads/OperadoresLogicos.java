package br.ufpr.tads;

import java.util.Scanner;

public final class OperadoresLogicos {

    static void inicia(){
        int bitUm;
        int bitDois;

        //pega valores dos bits
        Scanner scn = new Scanner(System.in);
        System.out.flush();
        System.out.println("Operadores lógicos: \n");
        System.out.println("Insira o valor do primeiro bit: \n");
        bitUm = scn.nextInt();
        System.out.println("Insira o valor do segundo bit: \n");
        bitDois = scn.nextInt();
        //exibe em tela
        exibeEmTela(bitUm, bitDois);
    }

    private static void exibeEmTela(int bitUm, int bitDois) {
        System.out.println("Operacao E -> " + String.valueOf(e(bitUm,bitDois)));
        System.out.println("Operacao OU -> " + String.valueOf(ou(bitUm,bitDois)));
        System.out.println("Operacao XOU -> " + String.valueOf(xOu(bitUm,bitDois)));
        System.out.println("Operacao NAO -> \n Bit um: " + String.valueOf(nao(bitUm))
                + "\n Bit dois: " + String.valueOf(nao(bitDois)));
        System.out.println("Operacao NAOE -> " + String.valueOf(naoE(bitUm, bitDois)));
        System.out.println("Operacao NAOOU -> " + String.valueOf(naoOu(bitUm, bitDois)));
    }

    private static int e(int bitUm, int bitDois) {
        return bitUm&bitDois;
    }

    private static int ou(int bitUm, int bitDois){
        return bitUm|bitDois;
    }

    private static int xOu(int bitUm, int bitDois){
        return bitUm^bitDois;
    }

    private static int nao(int bit){
        return ~bit;
    }

    private static int naoE(int bitUm, int bitDois){
        return ~(bitUm&bitDois);
    }

    private static int naoOu(int bitUm, int bitDois){
        return ~(bitUm|bitDois);
    }


}
