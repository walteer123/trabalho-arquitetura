package br.ufpr.tads;

import java.util.Scanner;

public final class ConversorDeBases {

    static void inicia(){
        int opcao;
        exibeMenuBases();
        Scanner scn = new Scanner(System.in);
        opcao = scn.nextInt();
        verificaOpcaoSelecionada(opcao, scn);
    }

    private static void verificaOpcaoSelecionada(int opcao, Scanner scn) {
        switch (opcao){
            case 1:
                System.out.println("Escreva o numero binario a ser convertido: \n");
                String bDec = scn.next();
                System.out.println(" Binario -> "+ bDec + "\n Decimal -> "+ binarioParaDecimal(bDec));
                break;
            case 2:
                System.out.println("Escreva o numero decimal a ser convertido: \n");
                int dBin = scn.nextInt();
                System.out.println(" Decimal -> "+ dBin + "\n Binario -> ");
                decimalParaBinario(dBin);
                System.out.println("\n");
                break;
            case 3:
                System.out.println("Escreva o numero decimal a ser convertido: \n");
                int dHex = scn.nextInt();
                System.out.println(" Decimal -> "+ dHex + "\n Hexadecimal -> " + decimalParaHexa(dHex));
                break;
            case 4:
                System.out.println("Escreva o numero hexadecimal a ser convertido: \n");
                String hDec = scn.next();
                System.out.println(" Hexadecimal -> "+ hDec + "\n decimal -> "+ hexaParaDecimal(hDec));
                break;
            case 5:
                System.out.println("Tchau!");
                System.exit(0);
                break;
            default:
                inicia();
                break;
        }
        inicia();
    }

    private static void exibeMenuBases() {
        System.out.println("Conversor de bases: \n");
        System.out.println("1.Binario para decimal \n");
        System.out.println("2.Decimal para binario \n");
        System.out.println("3.Decimal para hexadecimal \n");
        System.out.println("4.Hexadecimal para decimal \n");
        System.out.println("5.Sair \n");
        System.out.println("Selecione sua opcao: \n");
    }

    private static Integer binarioParaDecimal(String binario){
        char[] numeros = binario.toCharArray();
        Integer resultado = 0;
        int contador = 0;
        for(int i=numeros.length-1;i>=0;i--){
            if(numeros[i]=='1')
                resultado+=(int)Math.pow(2, contador);
            contador++;
        }
        return resultado;
    }

    private static void decimalParaBinario(int numero) {
        int restante;

        if (numero <= 1) {
            System.out.print(numero);
            return; // tiro da recursividade
        }

        restante = numero % 2;
        decimalParaBinario(numero >> 1);
        System.out.print(restante);
    }

    private static String decimalParaHexa(int numero){
        int meioByte = 0x0F;
        int tamanhoDoIntEmMeioByte = 8;
        int qtdBitsEmMeioByte = 4;
        char[] digitosHexa = {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F'
        };

        StringBuilder hexaMontado = new StringBuilder(tamanhoDoIntEmMeioByte); //8 eh o tamanho
        hexaMontado.setLength(tamanhoDoIntEmMeioByte);

        for (int i = tamanhoDoIntEmMeioByte - 1; i>=0; --i){
            int j = numero & meioByte;
            hexaMontado.setCharAt(i, digitosHexa[j]);
            numero >>= qtdBitsEmMeioByte;
        }
        return hexaMontado.toString();
    }

    private static int hexaParaDecimal(String hexaDecimal) {
        String digitosHexa = "0123456789ABCDEF";
        hexaDecimal = hexaDecimal.toUpperCase();
        int val = 0;
        for (int i = 0; i < hexaDecimal.length(); i++) {
            char c = hexaDecimal.charAt(i);
            int d = digitosHexa.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }

}
