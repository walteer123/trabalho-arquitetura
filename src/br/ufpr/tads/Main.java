package br.ufpr.tads;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int opcaoEscolhida = exibeMenu();
        switch (opcaoEscolhida){
            case 1:
                OperadoresLogicos.inicia();
                break;
            case 2:
                ConversorDeBases.inicia();
                break;
        }
    }

    private static int exibeMenu() {
        System.out.println("Trabalho Arquitetura: \n");
        System.out.println("----- MENU ----- \n");
        System.out.println("1.Operacoes logicas \n");
        System.out.println("2.Conversor de base \n");
        System.out.println("3.Somador 4 bits \n");
        System.out.println("Escreva a opcao desejada: \n");
        Scanner scn = new Scanner(System.in);
        return scn.nextInt();
    }



}
